<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public $timestamps = false;
    protected $fillable = [
       'path',
       'imageable_id',
       'imageable_type',
   ];
    public function imageable(){
       return $this->morphTo();
   }

   public function saveToUploads($file){
     $destinationFolder = 'uploads/images/';
     $filename = $file->getClientOriginalName();
     #$file->move_uploaded_file($file, $destinationFolder);
     $file->move($destinationFolder, $filename);
   }

}
