<?php

namespace App\Modules\Teams\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
  public function players(){
    return $this->hasMany('App\Modules\Players\Models\Player');
   }
}
