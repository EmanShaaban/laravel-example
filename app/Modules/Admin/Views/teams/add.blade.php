@extends('Admin::dashboard')

@section('title','Teams | Add')
@section('page-title','Add Team ')
@section('content')
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post"  action="/dashboard/teams/add" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <div class="box-body">
            <div class="form-group">
              <label for="clubName">Team Name</label>
              <input type="text" class="form-control" name="name" placeholder="Team Name" value="{{ old('name') }}">
            </div>
            <div class="form-group">
              <label for="profilePhoto">Team logo</label>
              <input type="file" id="exampleInputFile" name="team_logo">
              <p class="help-block">Example block-level help text here.</p>
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>


      <!-- /.box -->
      @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif

    </div>
    </div>
  </div>
@endsection
