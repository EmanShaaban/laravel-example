@extends('Admin::dashboard')

@section('content')
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="/dashboard/clubs/{{$club->id}}/edit" method ="PUT" enctype="multipart/form-data" >
          {!! csrf_field() !!}
          <div class="box-body">
            <div class="form-group">
              <label for="clubName">Club Name</label>
              <input type="text" class="form-control" name="name" placeholder="Club Name" value="{{ $club->name }}">
            </div
            <div class="form-group">
              <label for="profilePhoto">Profile photo</label>
              <input type="file" id="exampleInputFile" name="club_logo">
              <p class="help-block">Example block-level help text here.</p>
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <!-- /.box -->
      @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif

      </div>
    </div>
  </div>
@endsection
