@extends('Admin::dashboard')

@section('title','Players | Add')
@section('page-title','Add Player ')
@section('content')
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="{{ URL::current() }}" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <div class="box-body">
            <div class="form-group">
              <label for="playerName">Player Name</label>
              <input type="text" class="form-control" name="name" placeholder="Player Name" value="{{ old('name') }}">
            </div>
            <div class="form-group">
              <label for="playerClubId">Club ID</label>
              <input type="text" class="form-control" name="club_id" placeholder="Club ID" value="{{ old('club_id') }}">
            </div>
            <!-- Date -->
            <div class="form-group">
              <label>Birth Date</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" name="birth_date" id="datepicker">
              </div>
              <!-- /.input group -->
            </div>
            <!-- /.form group -->
            <div class="form-group">
              <label for="playerFavoritefoot">Favorite foot</label>
              <input type="text" class="form-control" name="favorite_foot" placeholder="Favorite foot" value="{{ old('favorite_foot') }}">
            </div>


            <div class="form-group">
              <label for="profilePhoto">Profile photo</label>
              <input type="file" id="exampleInputFile" name="profile_photo">
              <p class="help-block">Example block-level help text here.</p>
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <!-- /.box -->
      @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif

      </div>
    </div>
@endsection
