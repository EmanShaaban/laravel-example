
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">Navigation</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="{{ URL::to('dashboard/players') }}"><i class="fa fa-link"></i> <span>Players</span></a></li>
        <li><a href="{{ URL::to('dashboard/teams') }}"><i class="fa fa-link"></i> <span>Teams</span></a></li>
        <li><a href="{{ URL::to('dashboard/clubs') }}"><i class="fa fa-link"></i> <span>Clubs</span></a></li>

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
