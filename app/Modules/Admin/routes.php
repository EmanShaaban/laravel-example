<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it shoulCont responCont to
| anCont give it the AuthAuthController to call when that URI is requesteCont.
|
*/

Route::group(['middleware' => ['auth']], function()
{
  Route::get('dashboard','App\Modules\Admin\Controllers\AdminController@dash');

//Players
  Route::get('dashboard/players','App\Modules\Admin\Controllers\AdminController@index');
  Route::get('dashboard/players/add','App\Modules\Admin\Controllers\AdminController@addPlayer');
  Route::put('dashboard/players/{id}/edit','App\Modules\Admin\Controllers\AdminController@edit');
  Route::delete('dashboard/players/{id}','App\Modules\Admin\Controllers\AdminController@destroy');

//Teams
Route::post('dashboard/teams/add','App\Modules\Admin\Controllers\AdminTeamController@create');
Route::get('dashboard/teams/{id}/edit','App\Modules\Admin\Controllers\AdminTeamController@edit');
Route::get('dashboard/teams/{id}','App\Modules\Admin\Controllers\AdminTeamController@destroy');
// Clubs
Route::resource('dashboard/clubs','App\Modules\Admin\Controllers\AdminClubController');

});

/*Route::get('dashboard',['middleware' =>'auth','uses'=>'App\Modules\Admin\Controllers\AdminController@userInfo']);
#Route::get('admin/profile', ['middleware' => 'auth', 'App\Modules\Admin\Controllers\AdminController@user']);

Route::get('dashboard/players/add',['middleware' =>'auth','uses'=>'App\Modules\Admin\Controllers\AdminController@userInfo'], function(){
  return view('Admin::players.add');
});
Route::post('dashboard/players/add',['middleware' =>'auth','uses'=>'App\Modules\Admin\Controllers\AdminController@addPlayer']);

Route::post('edit/player/{id}', function(){

});*/




// Club routes
#Route::resource('dashboard/clubs',['middleware' =>'auth','uses'=>'App\Modules\Admin\Controllers\AdminClubController']);
/*Route::get('dashboard/clubs',['middleware' =>'auth','uses'=>'App\Modules\Admin\Controllers\AdminClubController@userInfo'], function(){
  return view('Admin::players.add');
});

Route::get('dashboard/clubs/add',['middleware' =>'auth','uses'=>'App\Modules\Admin\Controllers\AdminClubController@userInfo'], function(){
  return view('Admin::clubs.add');
});

Route::post('dashboard/players/add',['middleware' =>'auth','uses'=>'App\Modules\Admin\Controllers\AdminClubController@store'], function(){
  return view('Admin::clubs.add');
});*/
