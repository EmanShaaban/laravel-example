<?php

namespace App\Modules\Admin\Controllers;;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use View;
use App\Modules\teams\Models\Team;
use App\Photo;

class AdminTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        $photo = Photo::all()->where('imageable_type', 'App\Modules\teams\Models\Team');
        $data  =  array(
          'teams' => $teams,
          'photos'=> $photo
        );
        return View::make('Admin::teams.teams')
                  ->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('Admin::teams.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules = array(
            'name'       => 'required|unique:teams',
            'club_logo'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/teams/create')
                        ->withErrors($validator)
                        ->withInput();
        }else {
          $team = New Team;
          $team->name = $request->input('name');
          $team->save();

          $file = Input::file('club_logo')->getClientOriginalName();
          $photo = New Photo;
          $photo->path = $file;
          $photo->saveToUploads(Input::file('club_logo'));

          if($team->photos()->save($photo)){
            return Redirect::to('dashboard/teams');
          }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::find($id);
          return View::make('Admin::teams.edit')
            ->with('club', $team);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = array(
            'name'       => 'required'

        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/teams/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }else {
          $team = Team::find($id);
          $team->name = $request->input('name');
          $team->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
