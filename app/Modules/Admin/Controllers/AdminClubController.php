<?php

namespace App\Modules\Admin\Controllers;;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use View;
use App\Modules\Clubs\Models\Club;
use App\Photo;

class AdminClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clubs = Club::all();
        $photo = Photo::all()->where('imageable_type', 'App\Modules\Clubs\Models\Club');
        $data  =  array(
          'clubs' => $clubs,
          'photos'=> $photo
        );
        return View::make('Admin::clubs.clubs')
                  ->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('Admin::clubs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules = array(
            'name'       => 'required|unique:clubs',
            'club_logo'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/clubs/create')
                        ->withErrors($validator)
                        ->withInput();
        }else {
          $club = New Club();
          $club->name = $request->input('name');
          $club->save();

          $file = Input::file('club_logo')->getClientOriginalName();
          $photo = New Photo;
          $photo->path = $file;
          $photo->saveToUploads(Input::file('club_logo'));

          if($club->photos()->save($photo)){
            return Redirect::to('dashboard/clubs');
          }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $club = Club::find($id);
          return View::make('Admin::clubs.edit')
            ->with('club', $club);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = array(
            'name'       => 'required'

        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/clubs/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }else {
          $club = Club::find($id);
          $club->name = $request->input('name');
          $club->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
