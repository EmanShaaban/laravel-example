<?php

namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\Redirect;

use Validator;
use App\User as User;
use App\Modules\Players\Models\Player;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PhotoController as cont;
use App\Photo;
use View;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $players = Player::all();
      return View::make('Admin::players.players')
                  ->with('players',$players);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $player = Player::find($id);
       return View::make('Admin::players.edit')
                ->with('player',$player);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = array(
            'name'       => 'required',
            'club_id'      => 'required|unique:players',
            'birth_date' => 'required|date',
            'favorite_foot' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }else {
          $player = Player::find($id);
          $player->name = $request->input('name');
          $player->club_id = $request->input('club_id');
          $player->birth_date = $request->input('birth_date');
          $player->age = $player->playerAge($request->input('birth_date'));
          $player->favorite_foot = $request->input('favorite_foot');
          $player->save();

          $file = Input::file('profile_photo')->getClientOriginalName();
          $photo = New Photo;
          $photo->path = $file;
          $photo->saveToUploads(Input::file('profile_photo'));

          if($player->photos()->save($photo)){
            return Redirect::to('players');
          }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $player = Player::find($id);
      $player->delete();

      if($player->delete()){
      #  Session::flash('message', 'Successfully deleted the player!');
        return Redirect::to('dashboard/players');
      }
    }

    /*
    * Players CRUD functions
    *
    */
    public function addPlayer(Request $request) {
      $rules = array(
            'name'       => 'required',
            'club_id'      => 'required|unique:players',
            'birth_date' => 'required|date',
            'favorite_foot' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }else {
          $player = New Player;
          $player->name = $request->input('name');
          $player->club_id = $request->input('club_id');
          $player->birth_date = $request->input('birth_date');
          $player->age = $player->playerAge($request->input('birth_date'));
          $player->favorite_foot = $request->input('favorite_foot');
          $player->save();

          $file = Input::file('profile_photo')->getClientOriginalName();
          $photo = New Photo;
          $photo->path = $file;
          $photo->saveToUploads(Input::file('profile_photo'));

          if($player->photos()->save($photo)){
            return Redirect::to('players');
          }

        }

        // redirect
      //  Session::flash('message', 'Success!');
        //return Redirect::to('players');

    }

    public function dash() {
            return View::make('Admin::dashboard');
    }
}
