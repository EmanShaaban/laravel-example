<?php

namespace  App\Modules\Clubs\Models;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
  public $timestamps = false;

  public function photos(){
      return $this->morphMany('App\Photo', 'imageable');
  }
}
