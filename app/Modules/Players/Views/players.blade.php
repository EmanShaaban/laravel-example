@extends('index')


@section('content')
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered">
            <tr>
              <th style="width: 10px">#</th>
              <th>Player name</th>
              <th>Age</th>
              <th>Birth date</th>
              <th>Favorit foot</th>
              <th>Team</th>
            </tr>
            @foreach($players as $key => $value)
            <tr>
              <td>{{ $value->id }}</td>
              <td>{{ $value->name }}</td>
              <td>{{ $value->age }}</td>
              <td>{{ $value->birth_date }}</td>
              <td>{{ $value->favorite_foot }}</td>
              <td>{{ $value->team_id }}</td>
            </tr>
            @endforeach
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <li><a href="#">&laquo;</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">&raquo;</a></li>
          </ul>
        </div>
      </div>
      <!-- /.box -->
  </div>
@endsection
