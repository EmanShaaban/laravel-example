<?php

namespace App\Modules\Players\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public $timestamps = false;

    public function photos(){
        return $this->morphMany('App\Photo', 'imageable');
    }

    public function team() {
      return $this->belongsTo('App\Modules\Teams\Models\Team');
    }

    public function playerAge($birthDate) {
      $birthDate = explode("/", $birthDate);
      $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
        ? ((date("Y") - $birthDate[2]) - 1)
        : (date("Y") - $birthDate[2]));
      return $age;
    }
}
