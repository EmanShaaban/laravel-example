<?php

#use App\Modules\Players\Controllers\PlayerController as PlayerController;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('players', 'App\Modules\Players\Controllers\PlayerController@index');

Route::get('players/{id}', 'App\Modules\Players\Controllers\PlayerController@show');
