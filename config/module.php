<?php

return  [
    'modules' => [
       'Admin',
       'Players',
       'Teams',
       'Matches',
       'Clubs'
    ]
];
