<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Real Madrid</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset("/dist/css/bootstrap.min.css")}}" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{ asset("/assets/css/ie10-viewport-bug-workaround.css")}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset("/css/cover.css")}}" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{{ asset("/assets/js/ie-emulation-modes-warning.js")}}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">

              <nav>
                <ul class="nav masthead-nav">
                  <li class="active"><a href="{{ URL::to('/') }}">Home</a></li>
                  <li><a href="{{ URL::to('players') }}">Players</a></li>
                  <li><a href="#">Teams</a></li>
                  <li><a href="#">Matches</a></li>
                  <li><a href="#">Rankings</a></li>
                  <li><a href="{{ URL::to('auth/login') }}">Login</a></li>
                  <li><a href="{{ URL::to('auth/register') }}">Register</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <section class="content">
            <div class="row">
            <div class="col-md-12">
            @yield('content')
            </div>
          </div>
          </section>
      </div>



    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset("/assets/js/vendor/jquery.min.js")}}"><\/script>')</script>
    <script src="{{ asset("/dist/js/bootstrap.min.js")}}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset("/assets/js/ie10-viewport-bug-workaround.js")}}"></script>
  </body>
</html>
